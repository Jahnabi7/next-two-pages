const initialState = {
  items: [
    { id: 1, value: 0, price: 450, isAdded: false },
    { id: 2, value: 0, price: 450, isAdded: false },
    { id: 3, value: 0, price: 450, isAdded: false },
    { id: 4, value: 0, price: 450, isAdded: false },
    { id: 5, value: 0, price: 450, isAdded: false },
    { id: 6, value: 0, price: 450, isAdded: false },
    { id: 7, value: 0, price: 450, isAdded: false },
    { id: 8, value: 0, price: 450, isAdded: false },
  ],
  totalItems: [],
};
const itemActions = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_ITEM":
      items = [...this.state.items];
      const index = items.indexOf(item);
      items[index] = { ...item };
      items[index].value++;
      return {
        ...state,
        items,
      };
    case "REMOVE_ITEM":
      items = this.state.items.filter((c) => c.id !== item.id);
      /*totalItems = this.state.items.filter((c) => c.value > 0).length;*/
      return {
        ...state,
        items,
      };
    default:
      return state;
  }
};

export default itemActions;
