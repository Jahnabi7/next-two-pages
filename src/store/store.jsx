import { createStore } from "redux";
import rootReducer from "../reducers/index";
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(
  rootReducer,
  {
    itemActions: {
      items: [
        { id: 1, value: 0, price: 450, isAdded: false },
        { id: 2, value: 0, price: 450, isAdded: false },
        { id: 3, value: 0, price: 450, isAdded: false },
        { id: 4, value: 0, price: 450, isAdded: false },
        { id: 5, value: 0, price: 450, isAdded: false },
        { id: 6, value: 0, price: 450, isAdded: false },
        { id: 7, value: 0, price: 450, isAdded: false },
        { id: 8, value: 0, price: 450, isAdded: false },
      ],
      totalItems: [],
    },
  }, // sets initial state
  composeWithDevTools()
); // makes debugging through Redux Dev Tools possible);

export default store;
