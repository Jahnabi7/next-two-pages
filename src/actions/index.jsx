import { ADD_ITEM, REMOVE_ITEM } from "../constants/ActionTypes";

export const addItem = () => ({
  type: ADD_ITEM, // mandatory key
});

export const removeItem = () => ({
  type: REMOVE_ITEM, // mandatory key
});
