import DashStyles from "../styles/Dashboard.module.css";
import Link from "next/link";
import Graph from "../components/Graph";
import React, { Component } from "react";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [{ id: 1 }, { id: 2 }, { id: 3 }],
      items1: [
        { id: 1, title: "Your First React App" },
        { id: 2, title: "Full-stack Architecture" },
        { id: 3, title: "Data Science with Python" },
        { id: 4, title: "Deep Learning with Tensorflow and Keras" },
        { id: 5, title: "Advanced Deep Learning and Computer Vision" },
      ],
    };
  }
  displayCard() {
    return this.state.items.map((item) => (
      <div key={item.id} className="col-12 col-md-4">
        <div className={DashStyles.card}>
          <b>42%</b>
          <div className={DashStyles.Progress_icon}></div>
          <p>YOUR OVERALL PROGRESS</p>
        </div>
      </div>
    ));
  }
  content() {
    return this.state.items1.map((item) => (
    <div key={item.id} className={DashStyles.content}>
      <h5>{item.title}</h5>
      <div className={DashStyles.container + " " + DashStyles.outer}>
        <div className={DashStyles.container + " " + DashStyles.progress}>
          <div className={DashStyles.container + " " + DashStyles.bar}></div>
        </div>
        <span className="p-2">80%</span>
        <button type="button" className={DashStyles.btn}>
          CONTINUE LEARNING
        </button>
      </div>
     </div>
    ));
  }
  render() {
    return (
      <>
        <div className="container">
          <div className={DashStyles.container + " " + DashStyles.main}>
            <div className="row">
              <div className="col">
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                      <Link href="/">
                        <a>Home</a>
                      </Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link href="/">
                        <a>Library</a>
                      </Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link href="/">
                        <a>Data</a>
                      </Link>
                    </li>
                  </ol>
                </nav>
                <h4>Vendor and Customer Contracts</h4>
              </div>
              <div className="col">
                <div className="container text-right">
                  <button type="button" className={DashStyles.btn}>
                    CONTINUE LEARNING{" "}
                  </button>
                </div>
              </div>
            </div>
            <hr className="hr"></hr>
            <div className="row">
              <div className="col">
                <div className={DashStyles.container + " " + DashStyles.one}>
                  <img
                    src="/yellow-icon.svg"
                    alt="icon"
                    className="d-inline block mr-3"
                  />
                  <p className="d-inline block">
                    <b>You are on track with your progress</b>
                  </p>
                </div>
                <div className="container">
                  <Graph />
                  <div className="row">{this.displayCard()}</div>
                </div>
              </div>
              <div className="col">
                <div className="container">
                  <h5>
                    <b>Complete the following task to reach your goal</b>
                  </h5>
                  <p>Timeline: 19 Oct - 01 Nov</p>
                </div>
                <div className="container">
                <div className={DashStyles.contentMain}>
      <h5>Setting Up the Development Environment</h5>
      <div className={DashStyles.container + " " + DashStyles.outer}>
        <div className={DashStyles.container + " " + DashStyles.progress}>
          <div className={DashStyles.container + " " + DashStyles.bar}></div>
        </div>
        <span className="p-2">80%</span>
        <button type="button" className={DashStyles.btn}>
          CONTINUE LEARNING
        </button>
      </div>
     </div>
                  {this.content()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default Dashboard;
