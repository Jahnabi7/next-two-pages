import React from "react";
import NavStyles from "../styles/Navbar.module.css";
import Link from "next/link";
import Notify from "../components/Notification";
import { useSelector } from "react-redux";
import itemActions from "../reducers/itemActions";
import { addItem } from "../actions";

function Navbar() {
  const totalItems = useSelector((store) => store.totalItems);

  return (
    <>
      <Notify />
      <nav className="navbar navbar-expand-lg navbar-light sticky-top">
        <div className={NavStyles.container_navbar}>
          <Link href="/">
            <a className="navbar-brand">
              <img
                src="/upGradJeet-Logo.svg"
                height="40"
                alt="logo"
                className={NavStyles.logo}
              />
            </a>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#Navbar"
            aria-controls="Navbar"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="Navbar">
            <ul className="navbar-nav my-2 my-lg-0">
              <li className="nav-item">
                <Link href="/dashboard">
                  <a className="nav-link mr-4">
                    <strong>MY PROGRAM</strong>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/knowledge">
                  <a className="nav-link mr-4">
                    <strong>ABOUT EXAMS</strong>
                  </a>
                </Link>
              </li>

              <li className={NavStyles.cart}>
                <img src="/cart.png" alt="cart" width="30" />
                <span className="badge rounded-pill bg-primary text-white align-top">
                  {totalItems}
                </span>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
