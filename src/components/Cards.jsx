import React, { useState } from "react";
import { addItem } from "../actions";
import CardStyles from "../styles/Cards.module.css";
import { useSelector, useDispatch } from "react-redux";

function Cards() {
  const dispatch = useDispatch();
  /*const totalItems = useSelector((state) => {
    console.log(state.totalItems);
    return state.totalItems;
  });
  const items = useSelector((state) => {
    console.log(state.items);
    return state.items;
  });*/
  const [Items, setItems] = useState([
    { id: 1, value: 0, price: 450, isAdded: false },
    { id: 2, value: 0, price: 450, isAdded: false },
    { id: 3, value: 0, price: 450, isAdded: false },
    { id: 4, value: 0, price: 450, isAdded: false },
    { id: 5, value: 0, price: 450, isAdded: false },
    { id: 6, value: 0, price: 450, isAdded: false },
    { id: 7, value: 0, price: 450, isAdded: false },
    { id: 8, value: 0, price: 450, isAdded: false },
  ]);

  const displayButton = () => {
    return <button className={CardStyles.btn}>Add to Cart</button>;
  };

  const displayCard = () => {
    return Items.map((item) => (
      <div key={item.id} className="col-12 col-md-6 col-lg-4">
        <div className={CardStyles.card_inner}>
          <h3>Introduction to Programming</h3>
          <div className={CardStyles.star}></div>
          <p>
            4 Lorem ipsum, dolor sit amet consectetur adipisicing elit. Culpa
            quam doloribus dolorem minus, magnam,
          </p>
          <img
            src="/images_@2x.jpg"
            alt="card-img"
            className={CardStyles.image}
          />
          <div className={CardStyles.price_main}>
            <p>Price:</p>
            {displayButton()}
          </div>
          <div className={CardStyles.price}>
            <p className={CardStyles.text_muted}>₹ 500</p>
            &nbsp;&nbsp;<span className={CardStyles.strong}>₹ 450</span>
          </div>
        </div>
      </div>
    ));
  };

  return (
    <>
      <div className="row">{displayCard()}</div>
    </>
  );
}

export default Cards;
