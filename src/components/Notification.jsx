import React, { Component } from 'react';
import NavStyles from "../styles/Navbar.module.css";

class Notify extends Component {
  state = { isActive: true };
  handleHide = () => {
    this.setState({ isActive: false });
  };
  render() {
    return (<>
      {this.state.isActive?<div className={NavStyles.container + " " + NavStyles.notification}>
        <div className={NavStyles.container + " " + NavStyles.child}>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repudiandae,
          accusamus quasi! &nbsp;&nbsp;
          <button className={NavStyles.btn}>KNOW MORE</button>
          <button
          onClick={this.handleHide}
            type="button"
            className="close"
            aria-label="Close"
            aria-hidden="true"
          >
            &times;
          </button>
        </div>
      </div> : null}
      </>
    );
  }
}

export default Notify;
