import FooterStyles from "../styles/Footer.module.css";

export default function Footer() {
    return (<>
    <footer className="Footer">
    <hr className={FooterStyles.hr}></hr>
    <div className={FooterStyles.container +" "+ FooterStyles.footer}>
        <p>Powered by Upgrad</p>
    </div>
    </footer></>);
}