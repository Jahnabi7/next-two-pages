import React, { Component } from "react";
import BannerStyles from "../styles/Banner.module.css";
import Link from 'next/link'

class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <div className={BannerStyles.Banner + " " + BannerStyles.Image}>
          <img src="/images_@5x.webp" alt="Banner img" className={BannerStyles.Image}/>
          <div className={BannerStyles.container}>
                <h1>
                  1. Lorem ipsum, dolor sit <br/>amet consectetur<br/> adipisicing elit.
                </h1>
                <p>
                  2. Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br/>
                  Culpa quam doloribus dolorem minus, magnam,<br/> dignissimos
                  accusamus earum officia<br/> inventore commodi sequi!<br/> Praesentium
                  asperiores tempore eos<br/> similique, temporibus corrupti.
                  Ratione, aliquam?
                </p>
                <button type="button" className={BannerStyles.btn}>
                  EXPLORE COURSES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#707;

                </button>
                <p className="card-text pt-3">
                  Lorem ipsum, dolor sit amet<br/> consectetur adipisicing elit.
                </p>
                </div>
              </div>

        <div className={BannerStyles.Banner + " " + BannerStyles.Content}>
          <div className="container">
            <h1>
              <strong>Our Courses</strong>
            </h1>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Culpa
              quam doloribus
              <br /> dolorem minus, magnam, dignissimos accusamus earum officia
              inventore commodi
              <br /> sequi!
            </p>
            <div className="row">
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Assistant Engineer/Junior Engineer</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a>
                </Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Banking and Insurance</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a>
                </Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Defence/Police</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>GATE/PSU</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Railway</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Section Engineer/Graduate Engineer</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
              <div className="col-12 col-md-6">
                <Link href="/ssc"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Staff Selection Commission</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
              <div className="col-12 col-md-6">
              <Link href="/"><a className={BannerStyles.links}><div
                  className={BannerStyles.card}
                ><h3><strong>Teaching Entrance</strong></h3>
                →
                <p>Learn about Home Gate in an interactive course with quizzes!</p></div>
                </a></Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Banner;
