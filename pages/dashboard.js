import React, { Component } from 'react';
import Head from "next/head";
import Layout from "../src/Layouts/Layout";
import Navbar from "../src/components/Navbar";
import Dashboard from "../src/components/Dashboard";

class Program extends Component {
  render() {
    return (
      <>
        <Head>
          <title>Jeet dasboard</title>
          <link rel="icon" href="/favicon.ico"></link>
        </Head>
        <Layout>
          <Navbar totalItems={this.props.totalItems}/>
          <Dashboard />
        </Layout>
      </>
    );
  }
}

export default Program;
