import React, { Component } from "react";
import Head from "next/head";
import Layout from "../src/Layouts/Layout";
import Navbar from "../src/components/Navbar";
import Banner from "../src/components/Banner";
import Footer from "../src/components/Footer";

class Home extends Component {
  render() {
    return (
      <>
        <Head>
          <title>Jeet</title>
          <link rel="icon" href="/favicon.ico"></link>
        </Head>
        <Layout>
          <Navbar />
          <Banner />
          <Footer />
        </Layout>
      </>
    );
  }
}

export default Home;
