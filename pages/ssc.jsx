import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Navbar from "../src/components/Navbar";
import Link from "next/link";
import Cards from "../src/components/Cards";
import { connect } from "react-redux";
import { addItem, removeItem } from "../src/actions";



function Ssc() {
  return (
    <>
      <Navbar/>
      <div className="container">
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link href="/">
                <a>Banking and Insurance</a>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link href="/">
                <a>SSC CGL</a>
              </Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              Courses
            </li>
          </ol>
        </nav>
      </div>
      <div className="container">
        <header className="text-center">
          <h1>SSC CGL</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            <br /> Fugiat molestias fugit repellendus necessitatibus officia
            <br /> praesentium.
          </p>
        </header>
        <Cards/>
      </div>
    </>
  );
}

const mapStateToProps = state => ({
  items: state.itemActions.items,
  totalItems: state.itemActions.totalItems
});

export default connect(
  mapStateToProps,
  { addItem, removeItem }
)(Ssc);
